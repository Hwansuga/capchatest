﻿using ImageProcessor;
using ImageProcessor.Imaging;
using IronOcr;
using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapchaTest
{
    public partial class Form1 : Form
    {
        List<List<int>> listValue = new List<List<int>>();

        AdvancedOcr ocr = new AdvancedOcr();

        public Form1()
        {
            InitializeComponent();
        }
        System.Windows.Forms.OpenFileDialog openFileDialog;
        private void Form1_Load(object sender, EventArgs e)
        {
            listValue.Add(new List<int>() { 50,7,20,74,6,22});
            listValue.Add(new List<int>() { 50, 8, 15, 76, 8, 22 });
            listValue.Add(new List<int>() { 45, 7, 20, 74, 6, 22 });
            listValue.Add(new List<int>() { 50, 7, 22, 55, 30, 26 });
            listValue.Add(new List<int>() { 55, 7, 23, 47, 13, 22 });
            listValue.Add(new List<int>() { 52, 7, 27, 68, 18, 27 });
            listValue.Add(new List<int>() { 25, 6, 15, 76, 8, 22 });

            openFileDialog = new System.Windows.Forms.OpenFileDialog();
            openFileDialog.FileName = "openFileDialog";
            openFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog_FileOk);


            ocr.CleanBackgroundNoise = true;
            ocr.AcceptedOcrCharacters = "0123456789";

            //pictureBox1.Image = Image.FromFile("test.jpg");
        }

        public string Type1Ret(Image img_ , int numType_)
        {
            List<int> value = listValue[numType_];

            Mat scr = OpenCvSharp.Extensions.BitmapConverter.ToMat((Bitmap)img_);
            Mat binary = new OpenCvSharp.Mat();
            Cv2.CvtColor(scr, binary, OpenCvSharp.ColorConversionCodes.BGR2GRAY);
            Cv2.Threshold(scr, binary, value[0], 255, ThresholdTypes.Binary);

            ImageFactory factory = new ImageFactory();
            factory.Load(OpenCvSharp.Extensions.BitmapConverter.ToBitmap(binary));
            Rectangle rt = new Rectangle(2, 2, factory.Image.Width - 4, factory.Image.Height - 4);
            factory.Crop(rt);
            factory.GaussianBlur(value[1]);
            factory.Contrast(100);
            factory.GaussianSharpen(value[2]);

            binary = OpenCvSharp.Extensions.BitmapConverter.ToMat((Bitmap)factory.Image);
            Cv2.FastNlMeansDenoising(binary, binary, value[3], value[4], value[5]);

            return ocr.Read(OpenCvSharp.Extensions.BitmapConverter.ToBitmap(binary)).Text.Replace(" ", string.Empty); ;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenCvSharp.Mat scr = OpenCvSharp.Extensions.BitmapConverter.ToMat((Bitmap)pictureBox1.Image);
            OpenCvSharp.Mat binary = new OpenCvSharp.Mat();

            OpenCvSharp.Cv2.CvtColor(scr, binary, OpenCvSharp.ColorConversionCodes.BGR2GRAY);
            Cv2.Threshold(scr, binary, trackBar1.Value, 255, ThresholdTypes.Binary);

            pictureBox2.Image = OpenCvSharp.Extensions.BitmapConverter.ToBitmap(binary);
//             Cv2.Blur(binary, binary, new OpenCvSharp.Size(trackBar3.Value, trackBar3.Value));
//             Cv2.GaussianBlur(binary , binary , new OpenCvSharp.Size(trackBar2.Value, trackBar2.Value), 1.5f);


            ImageFactory factory = new ImageFactory();
            factory.Load(OpenCvSharp.Extensions.BitmapConverter.ToBitmap(binary));
            Rectangle rt = new Rectangle(2,2, factory.Image.Width -4 , factory.Image.Height-4);
            factory.Crop(rt);

            factory.GaussianBlur(trackBar2.Value);          
            //factory.GaussianSharpen(trackBar3.Value);
            factory.Contrast(100);

            pictureBox3.Image = factory.Image;

            factory.Load(factory.Image);
            factory.GaussianSharpen(trackBar3.Value);

            pictureBox4.Image = factory.Image;

            binary = OpenCvSharp.Extensions.BitmapConverter.ToMat((Bitmap)factory.Image);

            Cv2.FastNlMeansDenoising(binary, binary, trackBar4.Value, trackBar5.Value, trackBar6.Value);

            pictureBox5.Image = OpenCvSharp.Extensions.BitmapConverter.ToBitmap(binary);

            //Cv2.Threshold(binary, binary, trackBar7.Value, 255, ThresholdTypes.Binary);
            factory.Load(OpenCvSharp.Extensions.BitmapConverter.ToBitmap(binary));
            
            //factory.DetectEdges(new ImageProcessor.Imaging.Filters.EdgeDetection.Laplacian3X3EdgeFilter());
            //factory.Filter(ImageProcessor.Imaging.Filters.Photo.MatrixFilters.BlackWhite);

            pictureBox6.Image = factory.Image;
            //pictureBox5.Image = OpenCvSharp.Extensions.BitmapConverter.ToBitmap(binary);

            label_Type3.Text = ocr.Read(pictureBox6.Image).Text;
        }
        
        private void openFileDialog_FileOk(object sender, CancelEventArgs e)
        {
            Bitmap image = new Bitmap(openFileDialog.FileName);
            if (image != null)
            {
                System.IO.FileInfo fi = new System.IO.FileInfo(openFileDialog.FileName);
                pictureBox1.Image = image;
            }

            Stopwatch sw1 = new Stopwatch();
            sw1.Start();

            string ret = Type1Ret(image, 0);
            if (ret.Length == 4)
                ret = Type1Ret(image, 2);
            else if (ret.Length == 6)
                ret = Type1Ret(image, 6);
            else if (ret.Length == 7)
                ret = Type1Ret(image, 5);
            label_Type1.Text = ret;

            sw1.Stop();

            long ret1 = sw1.ElapsedMilliseconds;

            Stopwatch sw2 = new Stopwatch();
            sw2.Start();

            string sret2 = Type1Ret(image, 1);
            if (sret2.Length == 4)
                sret2 = Type1Ret(image, 3);
            else if (sret2.Length == 4)
                sret2 = Type1Ret(image, 4);
            label_Type2.Text = sret2;

            sw2.Stop();

            long ret2 = sw2.ElapsedMilliseconds;

            label_Ret.Text = "ret1 : " + ret1 + " / ret2 : " + ret2 + " / 합 : " + (ret1 + ret2);

            //image.Dispose();
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            label1.Text = trackBar1.Value.ToString();
        }

        private void trackBar2_Scroll(object sender, EventArgs e)
        {
            label2.Text = trackBar2.Value.ToString();
        }

        private void trackBar3_Scroll(object sender, EventArgs e)
        {
            label3.Text = trackBar3.Value.ToString();
        }

        private void trackBar4_Scroll(object sender, EventArgs e)
        {
            label4.Text = trackBar4.Value.ToString();
        }

        private void trackBar5_Scroll(object sender, EventArgs e)
        {
            label5.Text = trackBar5.Value.ToString();
        }

        private void trackBar6_Scroll(object sender, EventArgs e)
        {
            label6.Text = trackBar6.Value.ToString();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            label1.Text = trackBar1.Value.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            openFileDialog.ShowDialog();
        }
    }
}
